-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  jeu. 16 jan. 2020 à 12:03
-- Version du serveur :  10.4.10-MariaDB
-- Version de PHP :  7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `hackathon_edf4`
--

-- --------------------------------------------------------

--
-- Structure de la table `danger`
--

DROP TABLE IF EXISTS `danger`;
CREATE TABLE IF NOT EXISTS `danger` (
  `id` int(16) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `familledanger_id` int(16) NOT NULL COMMENT 'id famille de danger',
  `danger_id` int(16) NOT NULL COMMENT 'id danger',
  `danger` varchar(200) NOT NULL COMMENT 'Danger',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `danger`
--

INSERT INTO `danger` (`id`, `familledanger_id`, `danger_id`, `danger`) VALUES
(0, 1, 1, 'Perte d\'équilibre . glissade'),
(1, 1, 2, 'Faux mouvement'),
(2, 1, 4, 'Heurt'),
(3, 1, 6, 'Autres'),
(4, 2, 1, 'D\'une hauteur de moins de 3 m'),
(5, 3, 2, 'Charge difficile à manutentionner'),
(6, 4, 3, 'Instabilité de la charge'),
(7, 5, 1, 'Quatre roues < 3.5 t'),
(8, 5, 5, 'Autres'),
(9, 6, 5, 'Autres'),
(10, 7, 5, 'Eléments piquants ou tranchants, angles vifs,...'),
(11, 7, 6, 'Autres'),
(12, 9, 2, 'Produits chimiques hors CMR'),
(13, 9, 9, 'Autres'),
(14, 10, 1, 'Explosion'),
(15, 10, 5, 'Autres'),
(16, 11, 2, 'Circuit Basse Tension'),
(17, 11, 3, 'Circuit Haute Tension'),
(18, 11, 4, 'Circuit Induit'),
(19, 11, 8, 'Autres'),
(20, 12, 1, 'Fatigue visuelle due au manque ou à l\'excès'),
(21, 12, 2, 'Visibilité restreinte par manque d\'éclairage'),
(22, 15, 4, 'Autres'),
(23, 18, 1, 'Préparation du travail, informations sur le chantier, les locaux etc...'),
(24, 19, 5, 'Autres'),
(25, 20, 5, 'Animaux sauvages et domestiques'),
(26, 20, 10, 'Autres'),
(27, 21, 1, 'Risques Généraux');

-- --------------------------------------------------------

--
-- Structure de la table `famille_danger`
--

DROP TABLE IF EXISTS `famille_danger`;
CREATE TABLE IF NOT EXISTS `famille_danger` (
  `familledanger_id` int(50) NOT NULL AUTO_INCREMENT COMMENT 'id famille de danger',
  `famille_danger` varchar(200) NOT NULL COMMENT 'Famille de danger',
  PRIMARY KEY (`familledanger_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `famille_danger`
--

INSERT INTO `famille_danger` (`familledanger_id`, `famille_danger`) VALUES
(1, 'CHUTE DE PLAIN-PIED'),
(2, 'CHUTE DE HAUTEUR'),
(3, 'MANUTENTION MANUELLE'),
(4, 'MANUTENTION MECANIQUE'),
(5, 'CIRCULATION ET DEPLACEMENT (Service ET Trajet)'),
(6, 'EFFONDREMENT ET CHUTE D\'OBJETS'),
(7, 'MACHINES, OUTILS, APPAREILS ET OUVRAGES'),
(9, 'PRODUITS DANGEREUX, EMISSIONS, DECHETS'),
(10, 'INCENDIE EXPLOSION'),
(11, 'ELECTRICITE'),
(12, 'ECLAIRAGE'),
(15, 'MANQUE D\'HYGIENE'),
(18, 'ORGANISATION DU TRAVAIL'),
(19, 'FACTEURS HUMAINS'),
(20, 'DIVERS'),
(21, 'RISQUES GENERAUX');

-- --------------------------------------------------------

--
-- Structure de la table `sd`
--

DROP TABLE IF EXISTS `sd`;
CREATE TABLE IF NOT EXISTS `sd` (
  `ref_id` int(16) NOT NULL AUTO_INCREMENT COMMENT 'Référence',
  `date_detection` date NOT NULL COMMENT 'Date détection',
  `date_saisie` date NOT NULL COMMENT 'Date saisie',
  `etat` varchar(100) NOT NULL COMMENT 'Etat',
  `service` varchar(50) NOT NULL COMMENT 'Services',
  `situation_declarant` varchar(50) NOT NULL COMMENT 'Situation déclarant',
  `agent` varchar(50) NOT NULL COMMENT 'Agent',
  `famille_danger` varchar(200) NOT NULL COMMENT 'Famille de danger',
  `danger` varchar(200) NOT NULL COMMENT 'Danger',
  `titre` varchar(50) NOT NULL COMMENT 'Titre',
  `cause` varchar(200) NOT NULL COMMENT 'Cause',
  `description` varchar(400) NOT NULL COMMENT 'Description',
  `type_localisation` varchar(100) NOT NULL COMMENT 'Type de localisation',
  `site` varchar(200) NOT NULL COMMENT 'Site',
  `poste` varchar(200) NOT NULL COMMENT 'Poste',
  `lieu` varchar(400) NOT NULL COMMENT 'Lieu, Site, Adresse, PDL',
  `commune` varchar(200) NOT NULL COMMENT 'Commune',
  `commentaire_traitement` varchar(400) NOT NULL COMMENT 'Commentaire traitement immédiat',
  `mesures` varchar(200) NOT NULL COMMENT 'Mesures conservatoires',
  `date_creation` date NOT NULL COMMENT 'Date création',
  `latitude` varchar(100) NOT NULL COMMENT 'Latitude',
  `longitude` varchar(100) NOT NULL COMMENT 'Longitude',
  PRIMARY KEY (`ref_id`)
) ENGINE=InnoDB AUTO_INCREMENT=202050 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `sd`
--

INSERT INTO `sd` (`ref_id`, `date_detection`, `date_saisie`, `etat`, `service`, `situation_declarant`, `agent`, `famille_danger`, `danger`, `titre`, `cause`, `description`, `type_localisation`, `site`, `poste`, `lieu`, `commune`, `commentaire_traitement`, `mesures`, `date_creation`, `latitude`, `longitude`) VALUES
(202001, '2019-07-04', '2019-07-24', 'Action(s) en cours', 'SERVICE A', 'Agent', 'ALAIN', '2 - CHUTE DE HAUTEUR', '2.1 - Dune hauteur de moins de 3 m', '', '3 - Outils / Matériels', '', 'Site', 'CORSE - CENTRALE THERMIQUE DU VAZZIO', '', '', '', '', '', '2019-07-24', '', ''),
(202002, '2019-07-11', '2019-07-24', 'Cloturée', 'SERVICE A', 'Agent', 'ALBERT', '2 - CHUTE DE HAUTEUR', '2.1 - Dune hauteur de moins de 3 m', '', '3 - Outils / Matériels', '', 'Site', 'CORSE - CENTRALE THERMIQUE DU VAZZIO', '', '', '', '', '', '2019-07-24', '', ''),
(202003, '2019-07-12', '2019-07-24', 'Cloturée', 'SERVICE A', 'Agent', 'ANDRE', '12 - ECLAIRAGE', '12 - ECLAIRAGE', '', '3 - Outils / Matériels', '', 'Site', 'CORSE - CENTRALE THERMIQUE DU VAZZIO', '', '', '', '', '', '2019-07-24', '', ''),
(202004, '2019-07-24', '2019-07-24', 'Cloturée', 'SERVICE A', 'Agent', 'ANNE', '9 - PRODUITS DANGEREUX, EMISSIONS, DECHETS', '9.9 - Autres', '', '3 - Outils / Matériels', '', 'Site', 'CORSE - CENTRALE THERMIQUE DU VAZZIO', '', '', '', '', '', '2019-07-24', '', ''),
(202005, '2019-07-15', '2019-07-24', 'Cloturée', 'SERVICE A', 'Agent', 'BERNARD', '9 - PRODUITS DANGEREUX, EMISSIONS, DECHETS', '9.2 - Produits chimiques hors CMR', '', '3 - Outils / Matériels', '', 'Site', 'CORSE - CENTRALE THERMIQUE DU VAZZIO', '', '', '', '', '', '2019-07-24', '', ''),
(202006, '2019-07-24', '2019-07-24', 'Cloturée', 'SERVICE B', 'Agent', 'CATHERINE', '5 - CIRCULATION ET DEPLACEMENT (Service ET Trajet)', '5.5 - Autres', '', '5 - Contexte / Milieu', '', 'Site', 'CORSE - CENTRE 2 AVENUE IMPERATRICE EUGENIE', '', '', '', '', '', '2019-07-24', '', ''),
(202007, '2019-07-23', '2019-07-25', 'En attente daffectation', 'SERVICE C', 'Agent', 'CHARLES', '11 - ELECTRICITE', '11.4 - Circuit Induit', '', '3 - Outils / Matériels', '', 'Ailleurs', '', '', 'Cargèse sur la route de Piana', 'CARGESE', '', '', '2019-07-25', '', ''),
(202008, '2019-07-25', '2019-07-25', 'Cloturée', 'SERVICE D', 'Agent', 'CHRISTIAN', '18C - DIVERS', '18C.5 - Animaux sauvages et domestiques', '', '5 - Contexte / Milieu', '', 'Ailleurs', '', '', 'Avenue Napoléon 3', 'AJACCIO', '', '', '2019-07-25', '', ''),
(202009, '2019-07-27', '2019-07-29', 'Brouillon', 'SERVICE E', 'Agent', 'CHRISTOPHE', '12 - ECLAIRAGE', '12.1 - Fatigue visuelle due au manque ou à lexcès', '', '5 - Contexte / Milieu', '', 'Site', 'CORSE - FANGO', '', '', '', '', '', '1900-01-00', '', ''),
(202010, '2019-07-24', '2019-07-29', 'En attente daffectation', 'SERVICE C', 'Agent', 'CLAUDE', '5 - CIRCULATION ET DEPLACEMENT (Service ET Trajet)', '5.5 - Autres', '', '5 - Contexte / Milieu', '', 'Ailleurs', '', '', 'Ancienne gendarmerie de Pèri.', 'CARBUCCIA', '', '', '1900-01-00', '', ''),
(202011, '2019-07-29', '2019-07-29', 'Cloturée', 'SERVICE D', 'Agent', 'DANIEL', '5 - CIRCULATION ET DEPLACEMENT (Service ET Trajet)', '5.1 - Quatre roues < 3.5 t', '', '3 - Outils / Matériels', '', 'Site', 'CORSE - SITE GAZ DES MILELLI', '', '', '', '', '', '2019-07-29', '', ''),
(202012, '2019-07-29', '2019-07-29', 'Brouillon', 'SERVICE F', 'Agent', 'DAVID', '1 - CHUTE DE PLAIN-PIED', '1.1 - Perte déquilibre - glissade', '', '5 - Contexte / Milieu', '', 'Site', 'CORSE - ASPRETTO', '', '', '', '', '', '1900-01-00', '', ''),
(202013, '2019-07-30', '2019-07-30', 'Cloturée', 'SERVICE G', 'Agent', 'DIDIER', '1 - CHUTE DE PLAIN-PIED', '1.2 - Faux mouvement', '', '5 - Contexte / Milieu', '', 'Site', 'CORSE - CENTRE 2 AVENUE IMPERATRICE EUGENIE', '', '', '', '', '', '2019-07-30', '', ''),
(202014, '2019-07-30', '2019-07-30', 'Cloturée', 'SERVICE A', 'Agent', 'DOMINIQUE', '18C - DIVERS', '18C.10 - Autres', '', '1 - Pas de cause', '', 'Site', 'CORSE - TAC', '', '', '', '', '', '2019-07-30', '', ''),
(202015, '2019-07-16', '2019-07-30', 'En attente daffectation', 'SERVICE B', 'Agent', 'ERIC', '18B - FACTEURS HUMAINS', '18B.5 - Autres', '', '1 - Pas de cause', '', 'Ailleurs', '', '', 'bottacina', 'BASTELICACCIA', '', '', '2019-07-30', '', ''),
(202016, '2019-07-31', '2019-07-31', 'Action(s) en cours', 'SERVICE C', 'Agent', 'FRANCOIS', '18C - DIVERS', '18C.10 - Autres', '', '3 - Outils / Matériels', '', 'Poste source', '', 'CORSE - POSTE SOURCE - MOROSAGLIA', '', '', '', '', '2019-07-31', '', ''),
(202017, '2019-07-31', '2019-07-31', 'Cloturée', 'SERVICE D', 'Agent', 'FRANCOISE', '15 - MANQUE DHYGIENE', '15.4 - Autres', '', '9 - Comportement', '', 'Site', 'CORSE - AUTRE', '', '', '', '', '', '2019-07-31', '', ''),
(202018, '2019-07-20', '2019-07-31', 'Cloturée', 'SERVICE B', 'Agent', 'FREDERIC', '11 - ELECTRICITE', '11.8 - Autres', '', '9 - Comportement', '', 'Ailleurs', '', '', 'impasse entre Av.NAPOLEON III et Quai Saint Erasme qui mène au port', 'PROPRIANO', '', '', '2019-07-31', '', ''),
(202019, '2019-07-31', '2019-07-31', 'Cloturée', 'SERVICE B', 'Agent', 'GEORGES', '1 - CHUTE DE PLAIN-PIED', '1.1 - Perte déquilibre - glissade', '', '11 - Autre', '', 'Site', 'CORSE - CENTRE 2 AVENUE IMPERATRICE EUGENIE', '', '', '', '', '', '2019-07-31', '', ''),
(202020, '2019-08-01', '2019-08-01', 'Action(s) en cours', 'SERVICE C', 'Agent', 'GERARD', '11 - ELECTRICITE', '11.2 - Circuit Basse Tension', '', '3 - Outils / Matériels', '', 'Ailleurs', '', '', 'Ajaccio, Pietralba, devant la pharmacie nouvelle, avenue noel Franchini', 'AJACCIO', '', '', '2019-08-01', '', ''),
(202021, '2019-08-01', '2019-08-01', 'En attente daffectation', 'SERVICE C', 'Agent', 'GUY', '11 - ELECTRICITE', '11.8 - Autres', '', '11 - Autre', '', 'Ailleurs', '', '', '327 centre commercial U PAESE 2', 'GROSSETO-PRUGNA', '', '', '2019-08-01', '', ''),
(202022, '2019-08-01', '2019-08-01', 'Cloturée', 'SERVICE B', 'Agent', 'HENRI', '11 - ELECTRICITE', '11.8 - Autres', '', '3 - Outils / Matériels', '', 'Site', 'CORSE - FANGO', '', '', '', '', '', '2019-08-01', '', ''),
(202023, '2019-08-02', '2019-08-02', 'Brouillon', 'SERVICE C', 'Agent', 'ISABELLE', '18C - DIVERS', '18C.5 - Animaux sauvages et domestiques', '', '11 - Autre', '', 'Ailleurs', '', '', 'route du village de TASSO', 'TASSO', '', '', '1900-01-00', '', ''),
(202024, '2019-07-31', '2019-08-02', 'En attente daffectation', 'SERVICE C', 'Agent', 'JACQUES', '11 - ELECTRICITE', '11.2 - Circuit Basse Tension', '', '3 - Outils / Matériels', '', 'Ailleurs', '', '', 'Poste DP Campu di fiori PS Caldaniccia DP Mezzavia', 'AJACCIO', '', '', '2019-08-02', '', ''),
(202025, '2019-05-15', '2019-08-05', 'Cloturée', 'SERVICE C', 'Prestataire', 'JEAN', '4 - MANUTENTION MECANIQUE', '4.3 - Instabilité de la charge', '', '4 - Méthode de travail', '', 'Poste source', '', 'CORSE - POSTE SOURCE - ASPRETTO', '', '', '', '', '2019-08-05', '', ''),
(202026, '2019-07-31', '2019-08-05', 'Cloturée', 'SERVICE C', 'Agent', 'JEANNE', '7 - MACHINES, OUTILS, APPAREILS ET OUVRAGES', '7.5 - Eléments piquants ou tranchants, angles vifs,...', '', '3 - Outils / Matériels', '', 'Poste source', '', 'CORSE - POSTE SOURCE - CALDANICCIA', '', '', '', '', '2019-08-05', '', ''),
(202027, '2019-05-15', '2019-08-05', 'Cloturée', 'SERVICE C', 'Prestataire', 'JOSEPH', '11 - ELECTRICITE', '11.4 - Circuit Induit', '', '3 - Outils / Matériels', '', 'Poste source', '', 'CORSE - POSTE SOURCE - ASPRETTO', '', '', '', '', '2019-08-05', '', ''),
(202028, '2019-08-02', '2019-08-05', 'Cloturée', 'SERVICE D', 'Agent', 'LAURENT', '3 - MANUTENTION MANUELLE', '3.2 - Charge difficile à manutentionner', '', '3 - Outils / Matériels', '', 'Site', 'CORSE - STATION GAZ ARINELLA', '', '', '', '', '', '2019-08-05', '', ''),
(202029, '2019-08-05', '2019-08-05', 'Cloturée', 'SERVICE A', 'Agent', 'LOUIS', '1 - CHUTE DE PLAIN-PIED', '1.1 - Perte déquilibre - glissade', '', '3 - Outils / Matériels', '', 'Site', 'CORSE - CASTIRLA', '', '', '', '', '', '2019-08-05', '', ''),
(202030, '2019-08-02', '2019-08-05', 'Cloturée', 'SERVICE A', 'Agent', 'MARC', '19 - RISQUES GENERAUX', '19.1 - Risques Généraux', '', '3 - Outils / Matériels', '', 'Site', 'CORSE - ASPRETTO', '', '', '', '', '', '2019-08-05', '', ''),
(202031, '2019-08-06', '2019-08-06', 'Cloturée', 'SERVICE A', 'Agent', 'MARCEL', '6 - EFFONDREMENT ET CHUTE DOBJETS', '6.5 - Autres', '', '10 - Matière', '', 'Site', 'CORSE - SAMPOLO', '', '', '', '', '', '2019-08-06', '', ''),
(202032, '2019-08-06', '2019-08-06', 'Cloturée', 'SERVICE G', 'Agent', 'MARIE', '10 - INCENDIE EXPLOSION', '10.5 - Autres', '', '3 - Outils / Matériels', '', 'Site', 'CORSE - CENTRE 2 AVENUE IMPERATRICE EUGENIE', '', '', '', '', '', '2019-08-06', '', ''),
(202033, '2019-06-27', '2019-08-06', 'Cloturée', 'SERVICE C', 'Agent', 'MAURICE', '7 - MACHINES, OUTILS, APPAREILS ET OUVRAGES', '7.6 - Autres', '', '3 - Outils / Matériels', '', 'Poste source', '', 'CORSE - POSTE SOURCE - PIETROSELLA', '', '', '', '', '2019-08-06', '', ''),
(202034, '2019-08-06', '2019-08-06', 'Cloturée', 'SERVICE C', 'Agent', 'MICHEL', '11 - ELECTRICITE', '11.3 - Circuit Haute Tension', '', '11 - Autre', '', 'Ailleurs', '', '', 'RESIDENCES LES MINELLI', 'VILLE-DI-PIETRABUGNO', '', '', '2019-08-06', '', ''),
(202035, '2019-07-18', '2019-08-06', 'Cloturée', 'SERVICE C', 'Agent', 'NICOLAS', '3 - MANUTENTION MANUELLE', '3.2 - Charge difficile à manutentionner', '', '3 - Outils / Matériels', '', 'Poste source', '', 'CORSE - POSTE SOURCE - CALDANICCIA', '', '', '', '', '2019-08-06', '', ''),
(202036, '2019-07-15', '2019-08-07', 'Cloturée', 'SERVICE D', 'Agent', 'OLIVIER', '6 - EFFONDREMENT ET CHUTE DOBJETS', '6.5 - Autres', '', '3 - Outils / Matériels', '', 'Site', 'CORSE - STATION GAZ ARINELLA', '', '', '', '', '', '2019-08-07', '', ''),
(202037, '2019-08-07', '2019-08-07', 'Action(s) en cours', 'SERVICE B', 'Agent', 'PASCAL', '1 - CHUTE DE PLAIN-PIED', '1.6 - Autres', '', '1 - Pas de cause', '', 'Site', 'CORSE - CENTRE 2 AVENUE IMPERATRICE EUGENIE', '', '', '', '', '', '2019-08-07', '', ''),
(202038, '2019-08-07', '2019-08-07', 'Action(s) en cours', 'SERVICE D', 'Agent', 'PATRICK', '5 - CIRCULATION ET DEPLACEMENT (Service ET Trajet)', '5.5 - Autres', '', '5 - Contexte / Milieu', '', 'Ailleurs', '', '', 'Aux environs du 47 Rue du Vittulo, route de Saint Antoine', 'AJACCIO', '', '', '2019-08-07', '', ''),
(202039, '2019-08-06', '2019-08-07', 'Brouillon', 'SERVICE C', 'Agent', 'PAUL', '2 - CHUTE DE HAUTEUR', '2.1 - Dune hauteur de moins de 3 m', '', '3 - Outils / Matériels', '', 'Poste source', '', 'CORSE - POSTE SOURCE - PROPRIANO', '', '', '', '', '1900-01-00', '', ''),
(202040, '2019-08-08', '2019-08-08', 'Cloturée', 'SERVICE B', 'Agent', 'PHILIPPE', '11 - ELECTRICITE', '11.2 - Circuit Basse Tension', '', '3 - Outils / Matériels', '', 'Ailleurs', '', '', 'Propriano, 115 Quartier BARTACCIA', 'PROPRIANO', '', '', '2019-08-08', '', ''),
(202041, '2019-08-12', '2019-08-12', 'Cloturée', 'SERVICE G', 'Agent', 'PIERRE', '1 - CHUTE DE PLAIN-PIED', '1.1 - Perte déquilibre - glissade', '', '9 - Comportement', '', 'Site', 'CORSE - CENTRE 2 AVENUE IMPERATRICE EUGENIE', '', '', '', '', '', '2019-08-12', '', ''),
(202042, '2019-07-21', '2019-08-13', 'Cloturée', 'SERVICE A', 'Agent', 'RAYMOND', '1 - CHUTE DE PLAIN-PIED', '1.4 - Heurt', '', '3 - Outils / Matériels', '', 'Site', 'CORSE - CENTRALE THERMIQUE DU VAZZIO', '', '', '', '', '', '2019-08-13', '', ''),
(202043, '2019-07-21', '2019-08-13', 'Cloturée', 'SERVICE A', 'Agent', 'RENE', '11 - ELECTRICITE', '11.2 - Circuit Basse Tension', '', '3 - Outils / Matériels', '', 'Site', 'CORSE - CENTRALE THERMIQUE DU VAZZIO', '', '', '', '', '', '2019-08-13', '', ''),
(202044, '2019-07-27', '2019-08-13', 'Cloturée', 'SERVICE A', 'Agent', 'ROBERT', '1 - CHUTE DE PLAIN-PIED', '1.1 - Perte déquilibre - glissade', '', '3 - Outils / Matériels', '', 'Site', 'CORSE - CENTRALE THERMIQUE DU VAZZIO', '', '', '', '', '', '2019-08-13', '', ''),
(202045, '2019-07-28', '2019-08-13', 'Cloturée', 'SERVICE A', 'Agent', 'ROGER', '10 - INCENDIE EXPLOSION', '10.1 - Explosion', '', '2 - Compétence / Connaissance', '', 'Site', 'CORSE - CENTRALE THERMIQUE DU VAZZIO', '', '', '', '', '', '2019-08-13', '', ''),
(202046, '2019-07-29', '2019-08-13', 'Cloturée', 'SERVICE A', 'Agent', 'SERGE', '1 - CHUTE DE PLAIN-PIED', '1.1 - Perte déquilibre - glissade', '', '3 - Outils / Matériels', '', 'Site', 'CORSE - CENTRALE THERMIQUE DU VAZZIO', '', '', '', '', '', '2019-08-13', '', ''),
(202047, '2019-07-29', '2019-08-13', 'Cloturée', 'SERVICE A', 'Agent', 'STEPHANE', '6 - EFFONDREMENT ET CHUTE DOBJETS', '6.5 - Autres', '', '3 - Outils / Matériels', '', 'Site', 'CORSE - CENTRALE THERMIQUE DU VAZZIO', '', '', '', '', '', '2019-08-13', '', ''),
(202048, '2019-08-04', '2019-08-13', 'Cloturée', 'SERVICE A', 'Agent', 'THIERRY', '11 - ELECTRICITE', '11.2 - Circuit Basse Tension', '', '3 - Outils / Matériels', '', 'Site', 'CORSE - CENTRALE THERMIQUE DU VAZZIO', '', '', '', '', '', '2019-08-13', '', ''),
(202049, '2019-08-07', '2019-08-13', 'Cloturée', 'SERVICE A', 'Agent', 'YVES', '18A - ORGANISATION DU TRAVAIL', '18A.1 - Préparation du travail, informations sur le chantier, les locaux etc...', '', '3 - Outils / Matériels', '', 'Site', 'CORSE - CENTRALE THERMIQUE DU VAZZIO', '', '', '', '', '', '2019-08-13', '', '');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
