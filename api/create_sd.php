<?php 

require_once('../model/class-sd.php');
require_once('../model/class-famille_danger.php');

$sd = new TSD();

$danger = new TFamilleDanger();
$danger->getFromDbById($_POST['famille_danger']);

$sd->famille_danger = $danger->famille_danger;
$sd->danger = $_POST['danger'];
$sd->cause = $_POST['cause'];
$sd->description = $_POST['description'];
$sd->date_detection = $_POST['date_detection'];
$sd->date_creation = $_POST['date_creation'];
$sd->agent = $_POST['agent'];
$sd->service = $_POST['service'];
$sd->type_localisation = $_POST['type_localisation'];
$sd->site = $_POST['site'];


$sd->latitude = $_POST['latitude'];
$sd->longitude = $_POST['longitude'];
$sd->picture = $_POST['picture'];
$sd->insert();