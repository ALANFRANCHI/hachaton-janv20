<?php

require_once("datamodels.php");

class TFamilleDanger extends ObjectModel{

    protected $definition = array(
        'table' => 'famille_danger',
        'identifier' => 'familledanger_id',
        'fields' => array(
            'famille_danger' => array('type' => self::TYPE_STRING),
        )
    );

    var $familledanger_id;
    var $famille_danger;

    public function __construct($id = null){
        parent::__construct($id);
    }


}
