<?php

class TDatabase{

    public static function connect(){
        $dbHnd = new PDO('mysql:host=localhost;dbname=hackathon_edf4;charset=utf8mb4', 'root', 'root');
        $dbHnd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $dbHnd;
    }

    public static function excuteSqlQuery($sqlReq, $dbHandle = null) {

        $result = false;
        try {
            $dbHnd = ($dbHandle != null) ? $dbHandle : TDatabase::connect();
            if ($dbHnd != null) {
                $dbHnd = TDatabase::connect();
                $dbHnd->query($sqlReq);
                $result = true;
            }
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
            $result = false;
        }

        return $result;

    }

    public static function getData($table = '', $fields = "*", $criteria = '', $leftjoin = array(), $innerjoin = array(), $method = PDO::FETCH_ASSOC, $className = null){

        $result = array();
        try {
            $dbHnd = TDatabase::connect();
            if ($dbHnd != null) {
                if ($table != '') {
                    $sqlReq = "SELECT $fields FROM $table ";
                    if (count($leftjoin) > 0) {
                        foreach ($leftjoin as $lj)
                            $sqlReq .= " LEFT JOIN $lj";
                    }
                    if (count($innerjoin) > 0) {
                        foreach ($innerjoin as $ij)
                            $sqlReq .= " INNER JOIN $ij";
                    }
                    if ($criteria != '') {
                        $sqlReq .= " WHERE $criteria";
                    }
                    $stmt = $dbHnd->prepare($sqlReq);
                    if($method == PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE && $className != null){
                        $stmt->setFetchMode($method, $className);
                    }else{
                        $stmt->setFetchMode($method);
                    }
                    $stmt->execute();
                    $result = $stmt->fetchAll();
                }
            }

        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
            $result = null;
        }

        return $result;

    }

    public static function getDataAssoc($table = '', $fields = "*", $criteria = '', $leftjoin = array(), $innerjoin = array()){
        return TDatabase::getData($table, $fields, $criteria, $leftjoin, $innerjoin, PDO::FETCH_ASSOC);
    }

    public static function getDataKeyPair($table = '', $fields = "*", $criteria = '', $leftjoin = array(), $innerjoin = array()){
        return TDatabase::getData($table, $fields, $criteria, $leftjoin, $innerjoin, PDO::FETCH_KEY_PAIR);
    }

    public static function getDataClass($table = '', $fields = "*", $criteria = '', $leftjoin = array(), $innerjoin = array(), $className = null){
        return TDatabase::getData($table, $fields, $criteria , $leftjoin, $innerjoin, PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $className);
    }

}
