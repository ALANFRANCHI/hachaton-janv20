<?php

require_once("class-database.php");

date_default_timezone_set('Europe/Paris');

class ObjectModel{

    const TYPE_INT     = 1;
    const TYPE_BOOL    = 2;
    const TYPE_STRING  = 3;
    const TYPE_FLOAT   = 4;
    const TYPE_DATE    = 5;
    const TYPE_HTML    = 6;
    const TYPE_NOTHING = 7;
    const TYPE_SQL     = 8;

    protected $definition = array();

    public function __construct($id = null){

        $result = array();
        $identifier = $this->definition["identifier"];
        $fields = $this->definition['fields'];

        $this->{$identifier} = 0;
        foreach ($fields as $field => $options) {
            $this->{$field} = '';
            if (array_key_exists('type', $options)) {
                if ($options['type'] == self::TYPE_INT) $this->{$field} = 0;
                if ($options['type'] == self::TYPE_FLOAT) $this->{$field} = 0;
                if ($options['type'] == self::TYPE_BOOL) $this->{$field} = 0;
                if ($options['type'] == self::TYPE_DATE) $this->{$field} = null;
            }
        }

        return $result;

    }

    public function __toArray(){

        $result = array();
        $identifier = $this->definition["identifier"];
        $fields = $this->definition['fields'];

        $result[$identifier] = $this->{$identifier};
        foreach ($fields as $field => $options) {
            $result[$field] = $this->{$field};
        }

        return $result;

    }

    public function getId(){

        $identifier = $this->definition["identifier"];
        return $this->{$identifier};

    }

    public function getTableName(){

        return $this->definition['table'];

    }

    public function getClassName(){

        return get_class($this);

    }

    public function getFields(){

        $result = array();
        $fields = $this->definition['fields'];

        foreach ($fields as $field => $options) {
            $result[] = array(
                'field' => $field,
                'options' => $options,
            );
        }

        return $result;

    }

    public function getFromDbById($id){

        $result = false;
        try {
            $dbHnd = TDatabase::connect();
            if ($dbHnd != null) {
                $sqlReq = 'SELECT * FROM '.$this->definition["table"].' WHERE '.$this->definition["identifier"].' = :id';
                $stmt = $dbHnd->prepare($sqlReq);
                $stmt->setFetchMode(PDO::FETCH_INTO, $this);
                $stmt->bindParam(':id', $id, PDO::PARAM_INT);
                $stmt->execute();
                $res = $stmt->fetch();
                if (!($res == false)) $result = true;
            }
            return $result;
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
        return $result;

    }

    public function getFromDbByProperty($property, $value, $ignoreCase = false){

        $result = false;
        try {
            $dbHnd = TDatabase::connect();
            if ($dbHnd != null) {

                if ($ignoreCase) {
                    $sqlWhere = "UPPER($property) LIKE UPPER(:propvalue)";
                } else {
                    $sqlWhere = "$property LIKE :propvalue";
                }

                $sqlReq = 'SELECT * FROM '.$this->getTableName().' WHERE '.$sqlWhere;
                $stmt = $dbHnd->prepare($sqlReq);
                $stmt->setFetchMode(PDO::FETCH_INTO, $this);
                $stmt->bindParam(':propvalue', $value, PDO::PARAM_STR);
                $stmt->execute();
                $res = $stmt->fetch();
                if (!($res == false)) $result = true;

            }
            return $result;
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
        return $result;
    }

    public function save($dbHandle = null){
        return (int)$this->getId() > 0 ? $this->update($dbHandle) : $this->insert($dbHandle);
    }

    public function insert($dbHandle = null){

        $className = $this->getClassName();

        $result = false;
        try {
            $dbHnd = ($dbHandle != null) ? $dbHandle : TDatabase::connect();
            if ($dbHnd != null) {

                // Automatically fill dates
                if (property_exists($this, 'created_date'))
                    $this->created_date = date('Y-m-d H:i:s');

                $fields = $this->definition['fields'];
                $fieldsCount = count($fields);
                $identifier = $this->definition["identifier"];
                $fieldsSqlString = ($fieldsCount > 0) ? "$identifier, " : "$identifier";
                $valuesSqlString = ($fieldsCount > 0) ? ":$identifier, " : ":$identifier";

                $i = 0;
                foreach ($fields as $field => $options) {
                    $fieldsSqlString .= "$field";
                    if ($i < $fieldsCount - 1) $fieldsSqlString .= ', ';

                    $valuesSqlString .= ":$field";
                    if ($i < $fieldsCount - 1) $valuesSqlString .= ', ';

                    $i++;
                }

                $sqlReq = 'INSERT INTO '.$this->getTableName().' ('.$fieldsSqlString.') values ('.$valuesSqlString.')';

                $stmt = $dbHnd->prepare($sqlReq);
                $this->{$identifier} = 0;
                if ($stmt->execute($this->__toArray($this))) {
                    $resultId = $dbHnd->lastInsertId($identifier);
                    $this->{$identifier} = $resultId;
                    $result = true;
                }

            }
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }

        if (!$result)
            return false;

        return $result;

    }

    public function update($dbHandle = null){

        $className = $this->getClassName();

        $result = false;
        try {
            $dbHnd = ($dbHandle != null) ? $dbHandle : TDatabase::connect();
            if ($dbHnd != null) {

                // Automatically fill dates
                if (property_exists($this, 'updated_date'))
                    $this->updated_date = date('Y-m-d H:i:s');

                $fields = $this->definition['fields'];
                $fieldsCount = count($fields);
                $identifier = $this->definition["identifier"];
                $fieldsValues = ($fieldsCount > 0) ? "$identifier = :$identifier, " : "$identifier = :$identifier";

                $i = 0;
                foreach ($fields as $field => $options) {
                    $fieldsValues .= "$field = :$field";
                    if ($i < $fieldsCount - 1) $fieldsValues .= ', ';
                    $i++;
                }

                $sqlReq = 'UPDATE '.$this->getTableName().' SET '.$fieldsValues.' WHERE ('.$identifier.' = :'.$identifier.')';

                $stmt = $dbHnd->prepare($sqlReq);
                if ($stmt->execute($this->__toArray($this)))
                    $result = true;

            }
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }

        if (!$result)
            return false;

        return $result;

    }

    public function delete($dbHandle = null){

        $className = $this->getClassName();

        $result = false;
        try {
            $dbHnd = ($dbHandle != null) ? $dbHandle : TDatabase::connect();
            if ($dbHnd != null) {

                $identifier = $this->definition["identifier"];

                $sqlReq = 'DELETE FROM '.$this->getTableName().' WHERE '.$identifier.' = :id';
                $stmt = $dbHnd->prepare($sqlReq);
                $stmt->bindParam(':id', $this->{$identifier}, PDO::PARAM_INT);
                if ($stmt->execute())
                    $result = true;

            }
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }

        if (!$result)
            return false;

        return $result;

    }

}