<?php

require_once("datamodels.php");

class TDanger extends ObjectModel{

    protected $definition = array(
        'table' => 'danger',
        'identifier' => 'id',
        'fields' => array(
            'familledanger_id' => array('type' => self::TYPE_INT),
            'danger_id' => array('type' => self::TYPE_INT),
            'danger' => array('type' => self::TYPE_STRING),
        )
    );

    var $id;
    var $familledanger_id;
    var $danger_id;
    var $danger;

    public function __construct($id = null){
        parent::__construct($id);
    }


}
