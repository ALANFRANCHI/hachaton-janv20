<?php

require_once("datamodels.php");

class TSD extends ObjectModel{

    protected $definition = array(
        'table' => 'sd',
        'identifier' => 'ref_id',
        'fields' => array(
            'date_detection' => array('type' => self::TYPE_DATE),
            'date_saisie' => array('type' => self::TYPE_DATE),
            'etat' => array('type' => self::TYPE_STRING),
            'service' => array('type' => self::TYPE_STRING),
            'situation_declarant' => array('type' => self::TYPE_STRING),
            'agent' => array('type' => self::TYPE_STRING),
            'famille_danger' => array('type' => self::TYPE_STRING),
            'danger' => array('type' => self::TYPE_STRING),
            'titre' => array('type' => self::TYPE_STRING),
            'cause' => array('type' => self::TYPE_STRING),
            'description' => array('type' => self::TYPE_STRING),
            'type_localisation' => array('type' => self::TYPE_STRING),
            'site' => array('type' => self::TYPE_STRING),
            'poste' => array('type' => self::TYPE_STRING),
            'lieu' => array('type' => self::TYPE_STRING),
            'commune' => array('type' => self::TYPE_STRING),
            'commentaire_traitement' => array('type' => self::TYPE_STRING),
            'mesures' => array('type' => self::TYPE_STRING),
            'date_creation' => array('type' => self::TYPE_DATE),
            'latitude' => array('type' => self::TYPE_STRING),
            'longitude' => array('type' => self::TYPE_STRING),
            'picture' => array('type' => self::TYPE_STRING),
        )
    );

    var $ref_id;
    var $date_detection;
    var $date_saisie;
    var $etat;
    var $service;
    var $situation_declarant;
    var $agent;
    var $famille_danger;
    var $danger;
    var $titre;
    var $cause;
    var $description;
    var $type_localisation;
    var $site;
    var $poste;
    var $lieu;
    var $commune;
    var $commentaire_traitement;
    var $mesures;
    var $date_creation;
    var $latitude;
    var $longitude;
    var $picture;

    public function __construct($id = null){
        parent::__construct($id);
    }


}
