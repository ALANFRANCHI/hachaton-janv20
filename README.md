**LIEN DEMO : https://univ.lanfranchi-family.fr/hackathon-janv20**

Notre problématique durant ces deux jours, a été
d’optimiser l’acquisition des données de situation
dangereuse, pour ainsi permettre de faciliter le
traitement de ces dangers et ainsi éviter des accidents
aux employés.
Pour cela nous avons décidé d’afficher sur une carte
les positions des endroits
présentant une situation pouvant s’avérer dangereuse .
En prenant a parti les employés en tant que
signalisateur de problème nous aurons
une vision globale des risques présents sur nos sites et pourrons
ainsi optimiser nos systèmes pour
maintenir une sécurité optimale.
En décidant de créer une application web on permet un
accès simple et rapide a la signalisation de
situation dangereuse en utilisant l'
appareil photo de leur téléphone nous pouvons
localiser la position de la situation dangereuse et
avoir une idée visuelle du problème.